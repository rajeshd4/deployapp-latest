/**
 * 
 */
package com.dev.deployapp.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.dev.deployapp.domain.Topic;

/**
 * @author devendra.singh
 *
 */
public interface TopicRepository extends MongoRepository<Topic, String> {

}
