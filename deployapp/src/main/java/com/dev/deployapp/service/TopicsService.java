package com.dev.deployapp.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dev.deployapp.domain.Topic;
import com.dev.deployapp.repository.TopicRepository;

@Service
public class TopicsService {
	
	@Autowired
	TopicRepository topicRepository;

	/*
	 * private List<Topic> topicList = new ArrayList<>(Arrays.asList(
	 * 
	 * new Topic("_spring", "_Spring FrameWork", "_Spring Description"), new
	 * Topic("spring", "Spring FrameWork", "Spring Description"), new Topic("java",
	 * "Java FrameWork", "Java Description")
	 * 
	 * ));
	 */

	public List<Topic> getAllTopic() {
		// TODO Auto-generated method stub
		
		//return topicList;
		return topicRepository.findAll();
	}

	public Optional<Topic> getTopicById(String id) {
		// TODO Auto-generated method stub
		//return topicList.stream().filter(topic -> topic.getId().equals(id)).findFirst().get();
		return topicRepository.findById(id);
	}

	public void addTopic(Topic topic) {
		// TODO Auto-generated method stub
		System.out.println(" Adding topics in topicList.");
		//topicList.add(topic);
		topicRepository.save(topic);
	}

	public void updateTopic(Topic topic, String id) {
		// TODO Auto-generated method stub
		//int counter = 0;
		// List<Topic>  topicList = this.getAllTopic();
		//for(Topic topic1:topicList) {
		 topicRepository.deleteById(id);
			//if(topic1.getId().equals(id)) {
				//topicList.set(counter, topic);
		   // for(Topic topic1:topicList) {
		    	
		    	//if(topic1.getId().equals(id)) {
		    		//topic.setId(topic.getId());
		    		topicRepository.save(topic);
		    	//}
		    	
		    	
		  //  }
		        
			}
			//counter++;
	/*
	 * }
	 * 
	 * 
	 * }
	 */

	public void deleteTopic(String id) {
		// TODO Auto-generated method stub
		//topicList.removeIf(topic -> topic.getId().equals(id));
		topicRepository.deleteById(id);
	}



}
