/**
 * 
 */
package com.dev.deployapp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author devendra.singh
 *
 */
@Document(collection = "topic")
public class Topic {

	/**
	 * 
	 */
	
	private String id;
	
    private String name;
    private String description;
    
	public Topic() {
		// TODO Auto-generated constructor stub
	}
	
	public Topic(String id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    @Override
    public String toString() {
      return "Topic [id=" + id + ", name=" + name + ", description=" + description + "]";
    }

}
