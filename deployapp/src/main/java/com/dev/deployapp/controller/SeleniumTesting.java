package com.dev.deployapp.controller;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumTesting {
	
	
	WebDriver driver;

	
	public static void main(String args[]) {
		// TODO Auto-generated method stub
		
		SeleniumTesting st = new SeleniumTesting();
		st.LaunchBrouser();
		st.SearchInput();
		st.CloseBrouser();

	}
	
	public void LaunchBrouser() {
		
		//System.getProperty("webdriver.chrome.driver", "F:/Driver/chromedriver.exe");
		
		System.setProperty("webdriver.chrome.driver","driver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.amazon.in/");
		//driver.get("index.html");
		
		
	}
	
	public void SearchInput() {
		driver.findElement(By.id("twotabsearchtextbox")).sendKeys("iphone XR");
		driver.findElement(By.xpath("//input[@type='submit']")).click();
	}
	
	public void CloseBrouser() {
		//driver.quit();
	}
	
	

}
