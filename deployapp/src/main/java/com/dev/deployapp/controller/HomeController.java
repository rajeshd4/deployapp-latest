package com.dev.deployapp.controller;



import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
	
	
	@GetMapping("/home")
	public String home() {
		         
		return "<body><h1>Hello Devendra!, Welcome To Git Application. This is new App.</h1></body>";

	}

}
