package com.dev.deployapp;



import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.dev.deployapp.controller.HomeController;



@SpringBootTest
public class HomeControllerTests {
	
	private MockMvc mockMvc;
	
	@InjectMocks
	private HomeController homeController; 

	@BeforeEach
	public void setUp() throws Exception {
		
		mockMvc = MockMvcBuilders.standaloneSetup(homeController).build();
	}

	@Test
	public void testHomeController() throws Exception{
		//fail("Not yet implemented");
		
		mockMvc.perform(MockMvcRequestBuilders.get("/home"))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.content().string("<body><h1>Hello Devendra!, Welcome To Git Application. This is new App.</h1></body>"));
		
		
	}

}
