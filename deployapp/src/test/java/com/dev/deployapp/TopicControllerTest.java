/**
 * 
 */
package com.dev.deployapp;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.reflect.MethodUtils;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dev.deployapp.controller.TopicController;
import com.dev.deployapp.domain.Topic;
import com.dev.deployapp.service.TopicsService;

/**
 * @author devendra.singh
 *
 */
@SpringBootTest
public class TopicControllerTest {

	/**
	 * 
	 */

	@Mock
	TopicsService topicsService; 
	
	  @Test
	  public void allTopicConfiguration()
	  {
		  List<Method> method = MethodUtils.getMethodsListWithAnnotation(TopicController.class, RequestMapping.class);
		  assertNotNull(method);
		  assertNotNull(method.get(0).getAnnotation(RequestMapping.class));
	  }
	 @Test
	 public void testFinallTopic()
	 {
		 Topic topic1= new Topic("_spring", "_Spring FrameWork", "_Spring Description");
		 Topic topic2= new Topic("spring", "Spring FrameWork", "Spring Description");
		
		 List<Topic> topics = new ArrayList<Topic>();
		 topics.add(topic1);
		 topics.add(topic2);
		 
		 when(topicsService.getAllTopic()).thenReturn(topics);
		 
		 List<Topic> result = topicsService.getAllTopic();
		 assertThat(result.size()).isEqualTo(2);
		 assertThat(result.get(0).getId().equals(topic1.getId()));
		 assertThat(result.get(1).getId().equals(topic2.getId()));
		 
	 }

}
