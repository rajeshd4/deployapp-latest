package com.dev.deployapp;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
/*import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;*/

@RestController
public class TestNGTesting {

	//static HtmlUnitDriver htmlUnitDriver;
	WebDriver driver;
	HttpHeaders headers;
	private RestTemplate restTemplate;

	@BeforeEach
	public void beforeTest() {
		
		System.out.println("in Before Test");
		File chromeDriver = new File("/opt/chromedriver");
        System.setProperty("webdriver.chrome.driver", chromeDriver.getAbsolutePath());
        
        System.out.println("Driver Path : --------> "+chromeDriver.getAbsolutePath());
		//System.setProperty("webdriver.chrome.driver", "opt/chromedriver-80.0.3987.106");
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.addArguments("--headless", "window-size=1024,768", "--no-sandbox");
		chromeOptions.addArguments("--disable-dev-shm-usage");
		chromeOptions.addArguments("start-maximized");
		driver = new ChromeDriver(chromeOptions);
	//	driver = new ChromeDriver();
		//htmlUnitDriver = new HtmlUnitDriver();
		// System.out.println("in Before Test");
		System.out.println("in Before Test");
		this.restTemplate = new RestTemplate();
	}

	@AfterEach
	public void afterTest() throws Exception {

		System.out.println("in after test");
		Thread.sleep(3000);
		takeScreenshot();
		//htmlUnitDriver = null;

	}

	
	/*
	 * @Test public void testAddTopic() {
	 * 
	 * System.out.println("in test method Add topics"); // Back End URL // String
	 * addURL =
	 * "http://java-microservice-app1-1289745246.us-west-2.elb.amazonaws.com:9090/api/topics";
	 * String addURL=
	 * "http://java-microservice-app1-1289745246.us-west-2.elb.amazonaws.com:8080/add";
	 * headers = new HttpHeaders(); headers.add("Accept", "application/json");
	 * headers.add("Content-Type", "application/json"); String jsonBody =
	 * "{\"id\":\"TestNG\",\"name\":\"Testing Framework\",\"description\":\"TestNG is a Automation Frameword\"}"
	 * ; System.out.println("\n\n" + jsonBody); HttpEntity<String> entity = new
	 * HttpEntity<String>(jsonBody, headers); ResponseEntity<String> response =
	 * restTemplate.postForEntity(addURL, entity, String.class);
	 * Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
	 * System.out.println("Topic is Added successfully"); }
	 */
	/**
	 * Method to get Employee ID from REsponse body I have used Json Simple API for
	 * Parsing the JSON object
	 * 
	 * @param json
	 * @return
	 */
	public String getTopicIDFromResponse(String json) {
		JSONParser parser = new JSONParser();
		JSONObject jsonResponseObject = new JSONObject();
		Object obj = new Object();
		try {
			obj = parser.parse(json);
		} catch (org.json.simple.parser.ParseException e) {
			e.printStackTrace();
		}
		jsonResponseObject = (JSONObject) obj;
		String id = jsonResponseObject.get("id").toString();
		return id;
	}

	public void takeScreenshot() throws Exception {
	    try
	    {
		String timeStamp;
		File screenShotName;
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		screenShotName = new File(
				"./target/surefire-reports/screenshots/" + timeStamp + ".png");
		FileUtils.copyFile(scrFile, screenShotName);
		//String filePath = screenShotName.toString();
		//Reporter.log(filePath);
	    }
	    catch(Exception e)
	    {
	        
	    }
	}

	@org.junit.jupiter.api.Test
	public void testAllTopics() throws Exception {
		System.out.println("in test method all topics");
	//	System.setProperty("webdriver.chrome.driver", "opt/chromedriver-80.0.3987.106");
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.addArguments("--headless", "window-size=1024,768", "--no-sandbox");
		chromeOptions.addArguments("--disable-dev-shm-usage");
		chromeOptions.addArguments("start-maximized");
		driver = new ChromeDriver(chromeOptions);
		driver.get("http://java-microservice-app1-1289745246.us-west-2.elb.amazonaws.com:8080");
		//BufferedImage buff = Graphics2DRenderer.renderToImage(allTopicURL,800,1024);
		//URL url = new URL(allTopicURL);
		//BufferedImage img = ImageIO.read(url);
		//File file = new File("downloaded.jpg");
		//ImageIO.write(img, "jpg", file);
		//BufferedImage buff = ImageRenderer.renderToImage(file, "alltopic.png", 1024);
		//ImageIO.write(buff, "png", new File("alltopic.png"));
		// String bodyText = driver.findElement(By.tagName("body")).getText();
		// System.out.println("************" + bodyText);
		// Assert.assertTrue(bodyText.contains(bodyText));
	}

}
